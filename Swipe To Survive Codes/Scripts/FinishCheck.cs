using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishCheck : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] Confetti;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.FinishGame();
            Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.MediumImpact);
            foreach (var cft in Confetti)
            {
                cft.Play();
            }
        }
    }
}
