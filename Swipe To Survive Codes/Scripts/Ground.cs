using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            ObjectPooler.Instance.OpenParticleObject(collision.transform, "PunchParticle");
            Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.MediumImpact);
        }
    }
}
