using Lean.Touch;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Transform hips;
    [SerializeField] private float maxForce;
    [SerializeField] private float minForce;
    [SerializeField] private float force;
    
    private Rigidbody[] allRigidbodies;
    private Rigidbody hipsRb;
    private Animator animator;

    private bool isAlive = true;
    public bool PlayerIsAlive
    {
        get => isAlive;
        set
        {
            isAlive = value;
        }
    }
    private void Start()
    {
        allRigidbodies = hips.GetComponentsInChildren<Rigidbody>();
        animator = GetComponent<Animator>();
        hipsRb = hips.GetComponent<Rigidbody>();
    }
    public void DieAndForce(Vector3 dir , float substractDieForce )
    {
        if (!PlayerIsAlive)
        {
            return;
        }

        foreach (var rb in allRigidbodies)
        {
            rb.velocity =  Vector3.zero;
        }

        PlayerIsAlive = false;
        DisableControl();
        AddForce(dir , substractDieForce);
        UIManager.Instance.ShowPanel(PanelType.Lose);
    }
   
    public void AddForce(Vector3 dir, float dieForce)
    {
        hipsRb.velocity = new Vector3(0, dieForce * dir.y, dieForce * dir.x);
    }
    public void RagdollModeOn()
    {
        animator.enabled = false;
    }
    public void StartThrow()
    {
        RagdollModeOn();
        LeanTouch.OnFingerSwipe += HandleHead;
    }
    public void DisableControl()
    {
        LeanTouch.OnFingerSwipe -= HandleHead;
    }
    private void HandleHead(LeanFinger obj)
    {
        if (Input.GetMouseButtonUp(0))
        {
            
            var addForce = obj.GetStartScreenDistance(obj.LastScreenPosition) * force;
            Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.LightImpact);
            if (addForce > maxForce)
            {
                addForce = maxForce;
            }
            else if( addForce < minForce)
            {
                addForce = minForce;
            }

            var dir = (obj.LastScreenPosition - obj.StartScreenPosition).normalized / 10;

            hipsRb.velocity = new Vector3(0, addForce * dir.y, addForce * dir.x);
        }
    }
}
