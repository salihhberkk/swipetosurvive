using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [SerializeField] private GameObject cannon;
    [SerializeField] private Transform shotPos;

    [SerializeField] private float repeatTime;
    [SerializeField] private float fireForce;
    [SerializeField] private float waitForThrowTheBall;

    [SerializeField] private Vector3 ballScale;

    private void Start()
    {
        InvokeRepeating("ThrowTheBall", waitForThrowTheBall, repeatTime);
    }
    private void ThrowTheBall()
    { 
        ObjectPooler.Instance.OpenBallObject(shotPos , "Ball" , fireForce ,ballScale);
        ObjectPooler.Instance.OpenParticleObject(shotPos, "CannonParticle");
    }

}
