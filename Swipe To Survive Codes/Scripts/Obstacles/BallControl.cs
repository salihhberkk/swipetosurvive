using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BallControl : MonoBehaviour
{
    [SerializeField] private float scaleSpeed;
    [SerializeField] private float whenStartDownScaling;

    private void Start()
    {
        StartInvoke();
    }
    private void SmallerBall()
    {
        transform.DOScale(0.2f ,scaleSpeed).SetSpeedBased().
            OnComplete(() => { 
                gameObject.SetActive(false); 
            }); ;
    }
    public void StartInvoke()
    {
        Invoke("SmallerBall", whenStartDownScaling);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.MediumImpact);
            ObjectPooler.Instance.OpenParticleObject(collision.transform, "PunchParticle");
        }
    }
}
