using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxes : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ObjectPooler.Instance.OpenParticleObject(collision.transform, "PunchParticle");
            Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.MediumImpact);
        }
    }
}
