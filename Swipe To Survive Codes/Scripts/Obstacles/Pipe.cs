using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Pipe : MonoBehaviour
{ 
    public float animDuration;
    public Ease animEase;

    private void Start()
    {

        transform.DORotate(new Vector3(0, 180, 0), animDuration)
            .SetEase(animEase)
            .SetLoops(-1, LoopType.Incremental);
    }
}
