using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour
{
    [SerializeField] private bool lazer;
    [SerializeField] private float dieForce;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Transform collidedTransform = collision.transform;
            ObjectPooler.Instance.OpenParticleObject(collision.transform, "DeathParticle");

            Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.HeavyImpact);

            var dir = (collidedTransform.position - transform.position).normalized;
            if (lazer)
            {
                collision.gameObject.GetComponentInParent<PlayerMovement>().DieAndForce(-dir, dieForce);
            }
            else
            {
                collision.gameObject.GetComponentInParent<PlayerMovement>().DieAndForce(dir, dieForce);
            }
        }
    }
}
