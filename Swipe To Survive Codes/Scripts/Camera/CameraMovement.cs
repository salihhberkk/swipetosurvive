using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform target;
    public Vector3 offset;
    private float posY;

    void LateUpdate()
    {
        /*posY = Mathf.Clamp(playerHips.transform.position.y, -5,
            up.transform.position.y / 2 );*/
        posY = Mathf.Clamp(target.position.y, -5, 7);

        transform.position = new Vector3(offset.x, offset.y + posY, target.position.z + offset.z);
    }
}
