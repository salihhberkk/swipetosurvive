using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectPooler : MonoSingleton<ObjectPooler>
{
    [SerializeField] private List<ObjectPooledItem> itemsToPool;
    [SerializeField] private GameObject pooledObjectHolder;

    private List<GameObject> pooledObjects;

    private void Awake()
    {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPooledItem item in itemsToPool)
        {
            for (int i = 0; i < item.amountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objectToPool);
                obj.transform.SetParent(pooledObjectHolder.transform);
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }
    }
    public void OpenParticleObject(Transform parentTransform, string objectTag)
    {
        var particle = GetPooledObject(objectTag);
        particle.transform.position = parentTransform.transform.position;
        particle.SetActive(true);
        particle.GetComponent<ParticleSystem>().Play();

    }
    public void OpenBallObject(Transform parentTransform , string objectTag , float fireForce , Vector3 ballScale)
    {
        var ball = GetPooledObject(objectTag);
        ball.transform.position = parentTransform.transform.position;
        ball.GetComponent<BallControl>().StartInvoke();

        ball.transform.localScale = new Vector3(ballScale.x, ballScale.y, ballScale.z);
        ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        ball.SetActive(true);
        ball.GetComponent<Rigidbody>().velocity = parentTransform.forward.normalized * fireForce;
    }
    private GameObject GetPooledObject(string tag)
    {
        for (int i = pooledObjects.Count - 1; i > -1; i--)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].tag == tag)
            {
                return pooledObjects[i];
            }
        }
        //pooledObjects.First(o => o.activeInHierarchy && o.tag == tag);
        foreach (ObjectPooledItem item in itemsToPool)
        {
            if (item.objectToPool.tag == tag)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectToPool);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    obj.transform.SetParent(pooledObjectHolder.transform);
                    return obj;
                }
            }
        }
        return null;
    }

    
}